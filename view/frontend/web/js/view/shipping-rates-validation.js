define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/shipping-rates-validator',
        'Magento_Checkout/js/model/shipping-rates-validation-rules',
        'Shipwire_Shipping/js/model/shipping-rates-validator',
        'Shipwire_Shipping/js/model/shipping-rates-validation-rules'
    ],
    function (
        Component,
        defaultShippingRatesValidator,
        defaultShippingRatesValidationRules,
        shippingRatesValidator,
        shippingRatesValidationRules
    ) {
        'use strict';

        defaultShippingRatesValidator.registerValidator('shipwire', shippingRatesValidator);
        defaultShippingRatesValidationRules.registerRules('shipwire', shippingRatesValidationRules);

        return Component;
    }
);
