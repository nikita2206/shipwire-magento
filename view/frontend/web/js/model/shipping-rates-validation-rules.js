define(
    [],
    function () {
        'use strict';

        return {
            /**
             * Rules for specific shipping address fields.
             * @see http://devdocs.magento.com/guides/v2.0/howdoi/checkout/checkout_carrier.html
             * @returns {Object}
             */
            getRules: function () {
                return {
                    'postcode': {
                        'required': true
                    },
                    'country_id': {
                        'required': true
                    }
                };
            }
        };
    }
);
