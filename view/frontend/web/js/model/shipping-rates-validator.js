define(
    [
        'jquery',
        'mageUtils',
        'Shipwire_Shipping/js/model/shipping-rates-validation-rules',
        'mage/translate'
    ],
    function ($, utils, validationRules, $t) {
        'use strict';

        return {
            validationErrors: [],

            /**
             * Validate shipping address fields based on the shipping rates validation rules.
             * @see http://devdocs.magento.com/guides/v2.0/howdoi/checkout/checkout_carrier.html
             * @param {Object} address
             * @returns {Boolean}
             */
            validate: function (address) {
                var self = this;

                this.validationErrors = [];
                $.each(validationRules.getRules(), function (field, rule) {
                    var message;

                    if (rule.required && utils.isEmpty(address[field])) {
                        message = $t('Field ') + field + $t(' is required.');

                        self.validationErrors.push(message);
                    }
                });

                return !Boolean(this.validationErrors.length);
            }
        };
    }
);
