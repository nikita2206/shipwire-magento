<?php
namespace Shipwire\Shipping\Setup;

use Magento\Framework\Setup\UninstallInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Integration\Api\IntegrationServiceInterface;
use Magento\Integration\Model\Config;

class Uninstall implements UninstallInterface
{
    protected static $shipwireIntegrations = [
        'Shipwire Fulfillment Services',
        'Shipwire Fulfillment Services - Production (US)',
        'Shipwire Fulfillment Services - Production (EU)',
        'Shipwire Fulfillment Services - Beta (US)',
    ];

    /**
     * Integration service
     *
     * @var \Magento\Integration\Api\IntegrationServiceInterface
     */
    protected $_integrationService;

    /**
     * Integration config
     *
     * @var Config
     */
    protected $_integrationConfig;

    /**
     * @param Config $integrationConfig
     * @param \Magento\Integration\Api\IntegrationServiceInterface $integrationService
     */
    public function __construct(
        Config $integrationConfig,
        IntegrationServiceInterface $integrationService
    ) {
        $this->_integrationService = $integrationService;
        $this->_integrationConfig = $integrationConfig;
    }

    /**
     * {@inheritdoc}
     */
    public function uninstall(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        foreach (self::$shipwireIntegrations as $shipwireIntegration) {
            $integration = null;
            try {
                $integration = $this->_integrationService->findByName($shipwireIntegration);
                if ($integration && $integration->getId()) {
                    $this->_integrationService->delete($integration->getId());
                }
            } catch (\Exception $e) {
                // Ignore exception
            }
        }
        $setup->endSetup();
    }
}
