<?php
/**
 * Copyright 2016 Shipwire Inc. All rights reserved.
 * This file and its content is copyright of Shipwire Inc.
 * for use with the Shipwire software solution.
 * Any redistribution or reproduction of part or all of the
 * contents in any form is strictly prohibited.
 * You may not, except with our express written permission,
 * distribute or commercially exploit the content.
 * Nor may you transmit it or store it in any other website or
 * other form of electronic retrieval system.
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Shipwire_Shipping',
    __DIR__
);