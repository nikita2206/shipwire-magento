<?php

namespace Shipwire\Shipping\Model;

use Psr\Log\LoggerInterface;

/**
 * Simple request class that does curl call to Shipwire rate service.
 * This provides the ability to mock the request for unit testing.
 *
 * @package Shipwire\Shipping\Model
 */
class ShipwireRequest
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $_logger;

    /**
     * @var string
     */
    protected $_apiEndpoint;

    /**
     * ShipwireRequest constructor.
     *
     * @param \Psr\Log\LoggerInterface $logger
     * @param string                   $apiEndpoint
     */
    public function __construct(LoggerInterface $logger, $apiEndpoint)
    {
        $this->_apiEndpoint = $apiEndpoint;
        $this->_logger      = $logger;
    }

    /**
     * @param $rateRequestXml
     *
     * @return string
     */
    public function doRequest($rateRequestXml)
    {
        $curlSession = curl_init();
        curl_setopt($curlSession, CURLOPT_URL, $this->_apiEndpoint);
        curl_setopt($curlSession, CURLOPT_POST, true);
        curl_setopt(
            $curlSession, CURLOPT_HTTPHEADER,
            ['Content-Type: application/xml']
        );
        curl_setopt($curlSession, CURLOPT_POSTFIELDS, $rateRequestXml);
        curl_setopt($curlSession, CURLOPT_HEADER, false);
        curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlSession, CURLOPT_TIMEOUT, 60);
        $result = curl_exec($curlSession);
        if (!$result) {
            $this->_logger->error(get_class($this) .
                ': Curl error: ' . curl_error($curlSession));
        }
        curl_close($curlSession);

        return $result;
    }
}