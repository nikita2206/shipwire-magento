<?php
namespace Shipwire\Shipping\Model\Carrier;

use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory;
use Psr\Log\LoggerInterface;
use Magento\Shipping\Model\Rate\ResultFactory;
use Magento\Quote\Model\Quote\Address\RateResult\MethodFactory;
use Shipwire\Shipping\Model\ShipwireRequest;


/**
 * Shipwire real-time rating method for Magento
 * Main class to request the shipping methods and rates from the Shipwire
 * rating service and process them for consumption by Magento.
 *
 * Copyright (C) 2016, Shipwire Inc.
 */
class ShippingMethod extends \Magento\Shipping\Model\Carrier\AbstractCarrier
    implements \Magento\Shipping\Model\Carrier\CarrierInterface
{
    // This must be the group id in adminhtml/system.xml
    protected $_code = 'shipwire';

    protected $_source = 'Shipwire Shipping Module 2.0.5';

    /**
     * @var \Magento\Quote\Model\Quote\Address\RateRequest
     */
    protected $_rawRequest = null;

    /**
     * @var \Magento\Shipping\Model\Rate\ResultFactory
     */
    protected $_rateResultFactory;

    /**
     * @var \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory
     */
    protected $_rateMethodFactory;

    /**
     * @var \Shipwire\Shipping\Model\ShipwireRequest
     */
    protected $_shipwireRequest;

    /**
     * Override parent constructor adding 2 new parameters, $rateResultFactory
     * and $rateMethodFactory.
     * By providing the type-hint this triggers Magento 2's new automatic
     * dependency injection so an instance of these factories are
     * automatically passed to the constructor.
     *
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory
     *          $rateErrorFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory
     * @param \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory
     *          $rateMethodFactory
     * @param array $data
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ErrorFactory $rateErrorFactory,
        LoggerInterface $logger,
        ResultFactory $rateResultFactory,
        MethodFactory $rateMethodFactory,
        array $data = []
    ) {
        $this->_rateResultFactory = $rateResultFactory;
        $this->_rateMethodFactory = $rateMethodFactory;
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
    }

    /**
     * Collect and get rates
     * Main method called by Magento.
     *
     * @param \Magento\Quote\Model\Quote\Address\RateRequest $request
     *
     * @return \Magento\Framework\DataObject|bool|null
     * @api
     */
    public function collectRates(RateRequest $request)
    {
        $this->_rawRequest = $request;

        // Skip if not enabled
        if (!$this->getModuleConfigData('active')) {
            return false;
        }

        /*
         * Ensure we have a meaningful amount of information to rate with
         */
        $shipToCountry = $request->getDestCountryId();
        if (empty($shipToCountry)) {
            return false;
        }
        $requestItems = $request->getAllItems();
        if (count($requestItems) == 0) {
            return false;
        }

        $response = $this->_submitRequest($request);

        if (empty($response)) {
            /**
             * @var $error \Magento\Quote\Model\Quote\Address\RateResult\Error
             */
            $error = $this->_rateErrorFactory->create();
            $error->setCarrier($this->_code);
            $error->setCarrierTitle($this->getModuleConfigData('title'));
            $error->setErrorMessage('No shipping methods are available');

            return $error;
        }

        /** @var \Magento\Shipping\Model\Rate\Result $result */
        $result = $this->_rateResultFactory->create();

        foreach ($response as $rMethod) {
            $cost = $rMethod['amount'];

            /**
             * @var \Magento\Quote\Model\Quote\Address\RateResult\Method $method
             */
            $method = $this->_rateMethodFactory->create();

            $method->setCarrier($this->_code);
            $method->setCarrierTitle($this->getModuleConfigData('title'));

            // This hook allows pricing overrides, free shipping, handling, etc
            $price = $this->getMethodPrice($cost, $rMethod['code']);

            $method->setMethod($rMethod['code']);
            $method->setMethodTitle($rMethod['title']);
            $method->setCost($cost);
            $method->setPrice($price);

            $result->append($method);
        }
        $this->_result = $result;
        $this->_updateFreeMethodQuote($request);

        return $this->_result;
    }

    /**
     * Overrides parent method to use Shipwire specific free shipping settings
     * from Admin rather than the default Magento settings.
     *
     * @param        $cost
     * @param string $method
     *
     * @return float|string
     */
    public function getMethodPrice($cost, $method = '')
    {
        if ($method == $this->getModuleConfigData('free_method') &&
            $this->getModuleConfigData('free_shipping_enable') &&
            $this->getModuleConfigData('free_shipping_subtotal')
            <= $this->_rawRequest->getPackageValueWithDiscount()
        ) {
            $price = '0.00';
        } else {
            $price = $this->getFinalPriceWithHandlingFee($cost);
        }

        return $price;
    }

    /**
     * Helper method to get Shipwire Admin settings.
     *
     * @param string $configPath
     *
     * @return mixed
     */
    public function getModuleConfigData($configPath)
    {
        return $this->getConfigData($configPath);
    }

    /**
     * Overrides parent method to use Shipwire specific admin settings rather
     * than default Magento settings for free shipping settings.
     *
     * @param \Magento\Quote\Model\Quote\Address\RateRequest $request
     *
     * @return void
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function _updateFreeMethodQuote($request)
    {
        if ($request->getFreeMethodWeight() == $request->getPackageWeight()
            || !$request->hasFreeMethodWeight()
        ) {
            return;
        }

        $freeMethod = $this->getModuleConfigData($this->_freeMethod);
        if (!$freeMethod) {
            return;
        }
        $freeRateId = false;

        if (is_object($this->_result)) {
            foreach ($this->_result->getAllRates() as $i => $item) {
                if ($item->getMethod() == $freeMethod) {
                    $freeRateId = $i;
                    break;
                }
            }
        }

        if ($freeRateId === false) {
            return;
        }

        if ($request->getFreeMethodWeight() == 0
        ) {
            // If the entire order has no weight because it
            // is free then set the price to 0
            $this->_result->getRateById($freeRateId)->setPrice(0);
        }

    }

    /**
     * Get the handling fee for the shipping + cost
     * Overrides parent method to use Shipwire specific admin settings
     * for handling fees rather than default Magento settings.
     *
     * @param float $cost
     *
     * @return float final price for shipping method
     */
    public function getFinalPriceWithHandlingFee($cost)
    {
        $handlingFee  = $this->getModuleConfigData('handling_fee');
        $handlingType = $this->getModuleConfigData('handling_type');

        if (!$handlingType) {
            $handlingType = self::HANDLING_TYPE_FIXED;
        }

        $handlingAction = $this->getModuleConfigData('handling_action');
        if (!$handlingAction) {
            $handlingAction = self::HANDLING_ACTION_PERORDER;
        }

        return $handlingAction == self::HANDLING_ACTION_PERPACKAGE ?
            $this->_getPerpackagePrice(
                $cost,
                $handlingType,
                $handlingFee
            ) : $this->_getPerorderPrice(
                $cost,
                $handlingType,
                $handlingFee
            );
    }

    /**
     * Check if carrier has shipping tracking option available
     * Implements interface method. All Shipwire methods are trackable.
     *
     * @return boolean
     * @api
     */
    public function isTrackingAvailable()
    {
        return true;
    }

    /**
     * Get allowed shipping methods
     * Implements interface method. All Shipwire shipping methods
     * (i.e. UPS, USPS, FedEx, etc.)
     * are treated as a single Shipwire method, just different services.
     *
     * @return array
     * @api
     */
    public function getAllowedMethods()
    {
        return [$this->_code => $this->getConfigData('name')];
    }

    /**
     * Allows passing a mock request class for unit testing.
     *
     * @param \Shipwire\Shipping\Model\ShipwireRequest $shipwireRequest
     */
    public function setShipwireRequest($shipwireRequest)
    {
        $this->_shipwireRequest = $shipwireRequest;
    }

    /**
     * @return \Shipwire\Shipping\Model\ShipwireRequest
     */
    protected function getShipwireRequest()
    {
        if (!$this->_shipwireRequest) {
            $shipwireServer         =
                $this->getModuleConfigData('shipwire_server');
            $endPoint = Server::getServerEndpoint($shipwireServer);
            $this->_shipwireRequest = new ShipwireRequest(
                $this->_logger,
                $endPoint
            );
        }

        return $this->_shipwireRequest;
    }

    /**
     * Main method for requesting rates from Shipwire rating service.
     *
     * @param \Magento\Quote\Model\Quote\Address\RateRequest $requestVar
     *
     * @return array
     */
    private function _submitRequest(RateRequest $requestVar)
    {

        $shipwireAvailableServices =
            $this->getModuleConfigData('available_services');

        $rateRequestXml = $this->_buildRateRequestXml($requestVar);

        $shipwireRequest = $this->getShipwireRequest();

        $response = trim($shipwireRequest->doRequest($rateRequestXml->asXML()));

        $emptyRateResult = [];

        if (false === $response) {
            return $emptyRateResult;
        }

        $parser = xml_parser_create();

        $success =
            xml_parse_into_struct($parser, $response, $xmlVals, $xmlIndex);

        xml_parser_free($parser);

        if (!$success) {
            $this->_logger->error("Error parsing XML: $response - "
                . var_export($xmlVals, true));
            return [];
        }

        foreach ($xmlVals as $key) {
            if ($key['tag'] == 'STATUS') {
                if ($key['value'] != 'OK') {
                    foreach ($xmlVals as $key) {
                        if ($key['tag'] == 'ERRORMESSAGE') {
                            $this->_logger->error(
                                str_replace("\n", '', $key['value'])
                            );
                        }
                    }
                    return $emptyRateResult;
                }

            }
        }

        $code              = [];
        $method            = [];
        $cost              = [];
        $supportedServices = explode(',', $shipwireAvailableServices);

        foreach ($xmlVals as $key) {
            if ($key['tag'] == 'QUOTE' && $key['type'] == 'open'
                && $key['level'] == 4
            ) {
                $code[] = $key['attributes']['METHOD'];
            }
            if ($key['tag'] == 'SERVICE' && $key['type'] == 'complete'
                && $key['level'] == 5
            ) {
                $method[] = $key['value'];
            }
            if ($key['tag'] == 'COST' && $key['type'] == 'complete'
                && $key['level'] == 5
            ) {
                $cost[] = $key['value'];
            }
        }

        $la = count($code);
        $lb = count($method);
        $lc = count($cost);

        $rateResult = [];
        if ($la == $lb && $lb == $lc) {
            foreach ($code as $index => $value) {
                if (in_array($value, $supportedServices)) {
                    $rateResult[] = [
                        'code'   => $code[$index],
                        'title'  => $method[$index],
                        'amount' => $cost[$index]
                    ];
                }
            }
        }

        return $rateResult;
    }

    /**
     * Build the rate request XML
     *
     * @param \Magento\Quote\Model\Quote\Address\RateRequest $requestVar
     *
     * @return \SimpleXMLElement
     */
    protected function _buildRateRequestXml(RateRequest $requestVar)
    {
        $shipwireUsername = $this->getModuleConfigData('shipwire_email');
        $shipwirePassword = $this->getModuleConfigData('shipwire_password');

        $orderCurrencyCode = 'USD';
        /**
         * @var $orderCurrency \Magento\Directory\Model\Currency
         */
        $orderCurrency = $requestVar->getBaseCurrency();
        if (!empty($orderCurrency)) {
            $orderCurrencyCode = $orderCurrency->getCode();
        }

        $orderNumber = uniqid('magento', true);

        $shipToAddress   = $requestVar->getDestStreet();
        $shipToCity       = $requestVar->getDestCity();
        $shipToRegion     = $requestVar->getDestRegionCode();
        $shipToCountry    = $requestVar->getDestCountryId();
        $shiptoPostalCode = $requestVar->getDestPostcode();

        $rateRequestXml = new \SimpleXMLElement('<RateRequest/>');
        $rateRequestXml->addAttribute('currency', $orderCurrencyCode);
        $rateRequestXml->addChild('Username', $shipwireUsername);
        $rateRequestXml->addChild('Password', $shipwirePassword);
        $rateRequestXml->addChild('Source', $this->_source);

        $orderXml = $rateRequestXml->addChild('Order');
        $orderXml->addAttribute('id', $orderNumber);

        $addressInfoXml = $orderXml->addChild('AddressInfo');
        $addressInfoXml->addAttribute('type', 'ship');
        $addressInfoXml->addChild('Address1', $shipToAddress);
        $addressInfoXml->addChild('City', $shipToCity);
        $addressInfoXml->addChild('State', $shipToRegion);
        $addressInfoXml->addChild('Country', $shipToCountry);
        $addressInfoXml->addChild('Zip', $shiptoPostalCode);

        $requestItems = $requestVar->getAllItems();
        $this->_buildRequestItemXml($orderXml, $requestItems);

        return $rateRequestXml;
    }

    /**
     * Method for generating XML to send to Shipwire rating service.
     *
     * @param \SimpleXMLElement                 $orderXml
     * @param \Magento\Quote\Model\Quote\Item[] $requestItems
     */
    protected function _buildRequestItemXml($orderXml, array $requestItems)
    {
        $itemNum = 1;
        if (count($requestItems) > 0) {
            $itemIncluded = [];
            $index        = 0;
            $numItems     = count($requestItems);
            // First match up Configurable's with likely simples, mark off the
            // items as included or not included.
            // Any simple types left over will be included
            foreach ($requestItems as $requestItem) {
                if (!array_key_exists($index, $itemIncluded)
                    && $requestItem->getProduct()->getTypeId()
                        == 'configurable'
                ) { // See if we have a configurable SKU
                    // Next see if the next item is the same SKU, simple,
                    // and with quantity 1
                    if (array_key_exists($index + 1, $requestItems)
                        && $requestItems[$index + 1]->getProduct()->getTypeId()
                            == 'simple'
                        && $requestItems[$index + 1]->getSku() ==
                            $requestItem->getSku()
                        && $requestItems[$index + 1]->getQty() == 1
                    ) {
                        // Include the configurable
                        $itemIncluded[$index] = 1;
                        // Don't include the following simple
                        $itemIncluded[$index + 1] = 0;
                    } else {
                        $foundSimple = 0;
                        // If we can't find a simple one after then see if we
                        // have and matching simple that is not already used.
                        for ($i = 0; $i < $numItems; $i++) {
                            if ($requestItems[$i]->getProduct()->getTypeId()
                                    == 'simple'
                                && !array_key_exists($i, $itemIncluded)
                                // Only consider items that are otherwise
                                // not yet evaluated
                                && $requestItems[$i]->getSku()
                                    == $requestItem->getSku()
                                && $requestItems[$i]->getQty() == 1
                            ) {
                                // Include configurable
                                $itemIncluded[$index] = 1;
                                // Don't include matching simple
                                $itemIncluded[$i]     = 0;
                                $foundSimple          = 1;
                                break;
                            }
                        }
                        // If we can't find a matching simple then don't
                        // consider the configurable
                        if (!$foundSimple) {
                            // Don't include this configurable
                            $itemIncluded[$index] = 0;
                        }
                    }

                }
                $index++;
            }
            // Now spin through again using the $itemIncluded array to
            // determine what goes into rating
            $index = 0;
            foreach ($requestItems as $requestItem) {
                if (!array_key_exists($index, $itemIncluded)
                    || $itemIncluded[$index] == 1
                ) {
                    // We need detached singles
                    // (no array key, or included items)
                    $itemXml = $orderXml->addChild('Item');
                    $itemXml->addAttribute('num', $itemNum++);
                    $itemXml->addChild('Code', $requestItem->getSku());
                    $itemXml->addChild('Quantity', $requestItem->getQty());
                }
                $index++;
            }
        }
    }
}
