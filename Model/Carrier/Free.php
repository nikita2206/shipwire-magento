<?php
namespace Shipwire\Shipping\Model\Carrier;

class Free
{
    /**
     * List of free shipping methods in the administration settings.
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => '',
                'label' => 'Disabled'
            ],
            [
                'value' => 'GD',
                'label' => 'Ground Service (GD)'
            ],
            [
                'value' => '2D',
                'label' => 'Two Day Service (2D)'
            ],
            [
                'value' => '1D',
                'label' => 'One Day Service (1D)'
            ],
            [
                'value' => 'INTL',
                'label' => 'International Service (INTL)'
            ],
            [
                'value' => 'E-INTL',
                'label' => 'International Economy Service (E-INTL)'
            ],
            [
                'value' => 'PL-INTL',
                'label' => 'International plus Service (PL-INTL)'
            ],
            [
                'value' => 'PM-INTL',
                'label' => 'International Premium Service (PM-INTL)'
            ]
        ];
    }
}