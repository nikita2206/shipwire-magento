<?php

namespace Shipwire\Shipping\Model\Carrier;

class Server
{
    const DEFAULT_SERVER = 'us_prod';

    /**
     * @var array
     */
    protected static $serverEndpoints = [
        'us_prod' => 'https://api.shipwire.com/exec/RateServices.php',
        'eu_prod' => 'https://api.eu.shipwire.com/exec/RateServices.php',
        'us_beta' => 'https://api.beta.shipwire.com/exec/RateServices.php',
    ];

    /**
     * List of available rating servers in the administration settings.
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => 'us_prod',
                'label' => 'Production (US)'
            ],
            [
                'value' => 'eu_prod',
                'label' => 'Production (EU)'
            ],
            [
                'value' => 'us_beta',
                'label' => 'Beta (US)'
            ],
        ];
    }

    /**
     * @param string $serverName
     *
     * @return string
     * @throws \RuntimeException
     */
    public static function getServerEndpoint($serverName)
    {
        if (empty($serverName)) {
            $serverName = self::DEFAULT_SERVER;
        }
        if (empty(self::$serverEndpoints[$serverName])) {
            throw new \RuntimeException("Invalid Server Name: $serverName");
        }
        return self::$serverEndpoints[$serverName];
    }
}
