<?php
/***
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Shipwire\Shipping\Test\Unit\Model;

use Shipwire\Shipping\Model\Carrier\ShippingMethod;

//use Magento\SampleShippingProvider\Model\Carrier;

/**
 * Class ShippingMethodTest
 */
class ShippingMethodTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var ShippingMethod
     */
    protected $_model;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Shipping\Model\Rate\Result
     */
    protected $_rateResultFactory;

    /**
     * @var \Magento\Quote\Model\Quote\Address\RateRequest
     */
    protected $_rateRequest;

    /**
     * @var \Magento\Quote\Model\Quote\Address\RateRequest
     */
    protected $_rateResult;

    /**
     * @var \Magento\Quote\Model\Quote\Address\RateResult\Method
     */
    protected $_rateMethodFactory;

    /**
     * @var \Magento\Quote\Model\Quote\Address\RateResult\Method
     */
    protected $_rateResultMethod;

    /**
     * @var string
     */
    protected $_shippingDestCountryId = 'US';

    /**
     * List of available services
     *
     * @var string
     */
    protected $_availableServices = '1D,2D,GD,FT,INTL,E-INTL,PL-INTL,PM-INTL';

    /**
     * Mocked XML result from Shipwire Rating service
     *
     * @var string
     */
    protected $_mockShipwireResultXml;

    /**
     * @inheritdoc
     */
    protected function setUp()
    {
        $this->_scopeConfig =
            $this->getMockBuilder(
                'Magento\Framework\App\Config\ScopeConfigInterface'
            )
                ->getMockForAbstractClass();

        $rateErrorFactory =
            $this->getMockBuilder(
                'Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory'
            )
                ->disableOriginalConstructor()
                ->setMethods(['create'])
                ->getMock();

        $logger = $this->getMockBuilder('Psr\Log\LoggerInterface')
            ->getMockForAbstractClass();

        $this->_rateResultFactory =
            $this->getMockBuilder('Magento\Shipping\Model\Rate\ResultFactory')
                ->disableOriginalConstructor()
                ->setMethods(['create'])
                ->getMock();

        $this->_rateResult =
            $this->getMockBuilder('Magento\Shipping\Model\Rate\Result')
                ->disableOriginalConstructor()
                ->setMethods(null)
                ->getMock();

        $this->_rateMethodFactory = $this->getMockBuilder(
            'Magento\Quote\Model\Quote\Address\RateResult\MethodFactory'
        )
                ->disableOriginalConstructor()
                ->setMethods(['create'])
                ->getMock();

        $this->_rateResultMethod = $this->getMockBuilder(
            'Magento\Quote\Model\Quote\Address\RateResult\Method'
        )
                ->disableOriginalConstructor()
                ->getMock();

        $this->_rateResultFactory->expects($this->any())->method('create')->
        willReturn($this->_rateResult);

        $this->_rateMethodFactory->expects($this->any())->method('create')->
        willReturn($this->_rateResultMethod);

        $this->_model = new ShippingMethod(
            $this->_scopeConfig,
            $rateErrorFactory,
            $logger,
            $this->_rateResultFactory,
            $this->_rateMethodFactory
        );

        $this->_mockShipwireResultXml =
            file_get_contents('ShipwireResult.xml');
    }

    public function testGetAllowedMethods()
    {
        $name = 'In-Store Pickup';
        $code = 'shipwire';
        $this->_scopeConfig->expects($this->once())->method('getValue')
            ->willReturn($name);

        $methods = $this->_model->getAllowedMethods();

        $this->assertArrayHasKey($code, $methods);
        $this->assertEquals($name, $methods[$code]);
    }

    public function testCollectRatesNotActive()
    {
        /** @var \Magento\Quote\Model\Quote\Address\RateRequest $request */
        $request = $this->getMockBuilder(
            'Magento\Quote\Model\Quote\Address\RateRequest'
        )
            ->disableOriginalConstructor()
            ->getMock();

        $this->mockIsActive(false);
        $this->assertFalse($this->_model->collectRates($request));
    }

    public function testCollectRates()
    {
        $this->mockIsActive(true);
        $this->mockAvailableServices();
        $this->mockRateRequest();

        /** @var \Magento\Shipping\Model\Rate\Result $result */
        $result = $this->_model->collectRates($this->_rateRequest);

        $this->assertInstanceOf('Magento\Shipping\Model\Rate\Result', $result);
        $this->assertEquals(3, count($result->getAllRates()));
    }

    protected function mockIsActive($result)
    {
        $this->_scopeConfig
            ->expects($this->at(0))
            ->method('getValue')
            ->with('carriers/shipwire/active')
            ->willReturn($result);
    }

    protected function mockRateRequest()
    {
        $this->_rateRequest = $this->getMockBuilder(
            'Magento\Quote\Model\Quote\Address\RateRequest'
        )
            ->disableOriginalConstructor()
            ->setMethods(null)
            ->getMock();

        $this->_rateRequest->setDestCountryId($this->_shippingDestCountryId);

        $this->getMock('\Magento\Quote\Api\Data\CartItemExtensionInterface');

        /** @var \Magento\Quote\Model\Quote\Item $item */
        $item = $this->getMockBuilder('Magento\Quote\Model\Quote\Item')
            ->disableOriginalConstructor()
            ->getMock();

        $this->getMock('Magento\Catalog\Api\Data\ProductExtensionInterface');

        /** @var \Magento\Catalog\Model\Product $product */
        $product = $this->getMockBuilder('Magento\Catalog\Model\Product')
            ->disableOriginalConstructor()
            ->getMock();

        $item
            ->expects($this->at(0))
            ->method('getProduct')
            ->willReturn($product);

        $this->_rateRequest->setAllItems([$item]);

        /** @var \Shipwire\Shipping\Model\ShipwireRequest $shipwireRequest */
        $shipwireRequest = $this->getMockBuilder(
            'Shipwire\Shipping\Model\ShipwireRequest'
        )
            ->disableOriginalConstructor()
            ->getMock();

        $shipwireRequest
            ->expects($this->at(0))
            ->method('doRequest')
            ->willReturn($this->_mockShipwireResultXml);

        $this->_model->setShipwireRequest($shipwireRequest);
    }

    protected function mockAvailableServices()
    {
        $this->_scopeConfig
            ->expects($this->at(1))
            ->method('getValue')
            ->with('carriers/shipwire/available_services')
            ->willReturn($this->_availableServices);
    }
}
