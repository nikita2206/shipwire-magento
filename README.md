## Synopsis

An extension to add dynamic Shipwire carriers and shipping rates to your Magento checkout.

## Motivation

This provides integration with your Shipwire account to use the shipping methods and product properties configured in Shipwire.

## Technical feature

[Carrier class](Model/Carrier/ShippingMethod.php) implements CarrierInterface and represents a shipping carrier. Shipping carrier should have one or more shipping methods.
Shipping method should have carrier code, carrier title, method code, method title and price. Shipping method represented by \Magento\Quote\Model\Quote\Address\RateResult\Method class.
[Carrier class](Model/Carrier/ShippingMethod.php) has 'collectRates' method used to get applicable shipping methods.
[Service class](Model/Carrier/Service.php) has 'toOptionArray' method used to get the list of available shipping methods in the admin panel.
[Free class](Model/Carrier/Free.php) has 'toOptionArray' method used to get the list of available free shipping methods in the admin panel.
[Configuration](etc/config.xml) 'registers' [Carrier class](Model/Carrier/ShippingMethod.php) as a shipping carrier.
[system.xml](etc/adminhtml/system.xml) makes our module configurable in the admin panel.

## Installation

This module is intended to be installed using composer.  After including this component and enabling it, you can verify it is installed by going the backend at:

STORES -> Configuration -> ADVANCED/Advanced ->  Disable Modules Output

Once there check that the module name shows up in the list to confirm that it was installed correctly.

## Tests

Unit tests could be found in the [Test/Unit](Test/Unit) directory.

## Contributors

Shipwire Core team

## License

[Open Source License](LICENSE.txt)